### image repeater

Useful npm packages

aigle, awaiting, canvas-pixel-color, cheerio, image-js, image-to-slices
openfl, rangeslider.js, shelljs, slices, storew


Todo:

- [x] Eye dropper (whatcolouristhis)
- [x] Drag n drop (whatcolouristhis, dragtastic)
- [x] Half drop (halfdrop.js)
- [x] Color mask
- [x] Downsize large images on upload
- [ ] Show a thumbnail that keeps updating
- [x] Sliders (rangeslider)
- [ ] Image slicing/mirroring/stretching (konva)
- [x] asset bundler (parcel)

- [x] Color mask/eyedropper tool
- [x] Downsize large images on upload
- [x] Half drop
- [x] Image export
- [x] Responsive & web/mobile friendly w/ Twitter Bootstrap
- [ ] Image thumbnail that shows the selected region
- [ ] Image slicing/mirroring/stretching