'use strict'
/* eslint-disable camelcase,no-unused-vars */
/* globals $,jQuery,Image,FormData,XMLHttpRequest,dj,w2f_settings,alert */
var img_data = {}
var canvasElement
var maskCanvas
var virtualCtx
var maskCtx
var ctx
var $horizontalSlider
var $verticalSlider
var sections
var dropDirection
var dropCount
var virtualCanvas
var maskColor
var TLInc = $('#TLInc')
var TLBO = $('#TLBO')
var TCInc = $('#TCInc')
var TCBO = $('#TCBO')
var TRInc = $('#TRInc')
var TRBO = $('#TRBO')
var MLInc = $('#MLInc')
var MLBO = $('#MLBO')
var MRInc = $('#MRInc')
var MRBO = $('#MRBO')
var BLInc = $('#BLInc')
var BLBO = $('#BLBO')
var BCInc = $('#BCInc')
var BCBO = $('#BCBO')
var BRInc = $('#BRInc')
var BRBO = $('#BRBO')
var HF_initiate_Repeat_Creator = function () {
  var HFRepeatCreator = (function ($) {
    var init = function () { // initiate variables, cache DOM elements
      $(document).on('ready', function (e) { /** *** Set click events */
        canvasElement = document.createElement('canvas')
        maskCanvas = document.createElement('canvas')
        virtualCanvas = document.createElement('canvas') // Virtual Canvases
        canvasElement.id = 'repeat-creator-canvas'
        virtualCanvas.id = 'virtual-canvas'
        maskCanvas.id = 'mask-canvas'
        ctx = canvasElement.getContext('2d')
        virtualCtx = virtualCanvas.getContext('2d')
        maskCtx = maskCanvas.getContext('2d')
        $('#repeat-creator')[0].appendChild(canvasElement)
        $horizontalSlider = $('#horizontal-adjust')
        $verticalSlider = $('#vertical-adjust')
        var selectColor = false // State variables
        var resizeTimer
        var maskColor = ''
        var dropDirection = 'None'
        var dropCount = 0
        var sections = ['TL', 'TC', 'TR', 'ML', 'MC', 'MR', 'BL', 'BC', 'BR']
        $('#mask-color').on('click', function (e) { // Toggle Mask Color selection on
          e.preventDefault()
          selectColor = true
        })
        $('body').on('click', function (e) { // Set Mask Color, unhide sliders, toggle Mask Color selection off
          if (e.target.id === 'repeat-creator-canvas' && selectColor) {
            maskColor = e.target.getContext('2d').getImageData(e.offsetX, e.offsetY, 1, 1).data
            var rgba = 'rgba(' + maskColor.join(',') + ')'
            if (rgba === 'rgba(0,0,0,0)') {
              rgba = $('#repeat-creator-canvas').css('background-color').replace('rgb(', 'rgba(').replace(')', ', 255)')
              maskColor = rgba.replace('rgba(', '').replace(')', '').split(', ')
            }
            $('#mask-color').css('background-color', rgba)
            $('#repeat-creator canvas').css('background-color', rgba)
            $horizontalSlider.parent().removeClass('hidden')
            $verticalSlider.parent().removeClass('hidden')
            selectColor = false
            drawMaskCanvas(maskColor) // Redraw canvas (mask)
          }
        })
        $('#select-image').click(function (e) {
          $('#img-upload-wrap').collapse('toggle')
        })
        $('#img-upload-wrap').on('show.bs.collapse', function (e) {
          $('#select-image small').text('Hide My Designs')
        })
        $('#img-upload-wrap').on('hide.bs.collapse', function (e) {
          $('#select-image small').text('Show My Designs')
        })
        $('.user-images').click(function (e) { // User image click
          var target = $(e.target)
          if (target.is('input[type="radio"]')) {
            img_data.activeImage = $(target).data('thumb') // Load image to canvas
            img_data.$selected = $(target)
            loadImage()
          }
        })
        $('.repeat-adjust').click(function (e) { // Slider button click
          var target = $(e.target)
          if (target.is('button')) {
            e.preventDefault()
            if (target.hasClass('increment')) {
              target.siblings('input').val(function (index, value) {
                return parseInt(value) + parseInt($(this).attr('step'))
              })
              $horizontalSlider.trigger('change')
            } else if (target.hasClass('decrement')) {
              target.siblings('input').val(function (index, value) {
                return parseInt(value) - parseInt($(this).attr('step'))
              })
              $horizontalSlider.trigger('change')
            }
          }
        })
        $('#download-repeat').click(function (e) {
          saveToPNG(this)
        })
        $('#save-repeat').click(function (e) {
          e.preventDefault()
          saveToServer()
        })
        $('#repeat-drop').on('change', function (e) { // Change drop value
          var $selectedOption = $(this).find('option:selected')
          dropDirection = $selectedOption.data('offset')
          dropCount = $selectedOption.data('value')
          drawToCanvas(ctx, canvasElement, true) // Redraw canvas (DROP change)  // master canvas is drawn
        }) // Slider changes
        $horizontalSlider.on('change input', function (e) {
          drawToCanvas(ctx, canvasElement, true)
        }) // master canvas is drawn
        $verticalSlider.on('change input', function (e) {
          drawToCanvas(ctx, canvasElement, true)
        }) // master canvas is drawn
        $(window).on('resize', function (e) {
          clearTimeout(resizeTimer)
          resizeTimer = setTimeout(redraw_canvas, 100)
        })
      })
    }
    var setActiveImg = function (image) {
      img_data.activeImage = image.data('thumb')
      img_data.$selected = image
    }
    var initiate_canvas = function () {
      loadImage()
    }
    var resize_canvas = function () {
      if (img_data.width > img_data.height) { // Resize canvas based on window / image size
        img_data.scaled_width = (window.innerWidth * 0.75)
        img_data.scale = img_data.scaled_width / img_data.width
        img_data.scaled_height = img_data.height * img_data.scale
        if (img_data.scaled_height > window.innerHeight) { // Re-adjust in case the height becomes more than the window's height
          img_data.scaled_height = window.innerHeight * 0.8 - $('#repeat-creator-toolbar').innerHeight()
          img_data.scale = img_data.scaled_height / img_data.height
          img_data.scaled_width = img_data.width * img_data.scale
        }
      } else {
        if (window.innerWidth > window.innerHeight) {
          img_data.scaled_height = (window.innerHeight * 0.85) - $('#repeat-creator-toolbar').innerHeight()
          img_data.scale = img_data.scaled_height / img_data.height
          img_data.scaled_width = img_data.width * img_data.scale
        } else {
          img_data.scaled_width = (window.innerWidth * 0.75)
          img_data.scale = img_data.scaled_width / img_data.width
          img_data.scaled_height = img_data.height * img_data.scale
        }
      }
      $(canvasElement).width(img_data.scaled_width + 60)
      $(canvasElement).height(img_data.scaled_height + 60)
      initiate_slider_values()
      canvasElement.width = img_data.scaled_width + 60
      canvasElement.height = img_data.scaled_height + 60
    }
    var loadImage = function () {
      var img = new Image()
      img.crossOrigin = 'Anonymous'
      resetMaskColor()
      img.onload = function () { // Store image data
        img_data.width = img_data.$selected.data('width')
        img_data.height = img_data.$selected.data('height')
        resize_canvas()
        virtualCanvas.width = img_data.$selected.data('width')
        virtualCanvas.height = img_data.$selected.data('height')
        maskCanvas.width = img_data.$selected.data('width')
        maskCanvas.height = img_data.$selected.data('height')
        virtualCtx.drawImage(this, 0, 0, img_data.$selected.data('width'), img_data.$selected.data('height')) // virtualCanvas &
        maskCtx.drawImage(this, 0, 0, img_data.$selected.data('width'), img_data.$selected.data('height')) // maskCanvas both get image drawn to them
        drawToCanvas(ctx, canvasElement, true) // master canvas
        reposition_sliders()
      }
      img.src = img_data.activeImage
    }
    var drawToCanvas = function (context, canvas, clear, scaled, callback) {
      var top, left
      var imgheight = img_data.scaled_height
      var imgwidth = img_data.scaled_width
      var offsetX = (img_data.scaled_width - ($horizontalSlider.val()) * img_data.scale) / 2
      var offsetY = (img_data.scaled_height - ($verticalSlider.val()) * img_data.scale) / 2
      if (!scaled && scaled !== undefined) {
        imgheight = img_data.height
        imgwidth = img_data.width
        offsetX = (imgwidth - $horizontalSlider.val()) / 2
        offsetY = (imgheight - $verticalSlider.val()) / 2
      }
      clear ? clearCanvas(context, canvas.width, canvas.height) : clearCanvas(context, canvas.width, canvas.height)
      var doVertDrop = false
      var doHorDrop = false
      var drawThis = false
      for (var section in sections) {
        switch (sections[section]) {
          case 'TL':
            top = ((0 - imgheight) + (offsetY * 2)) + 30
            left = ((0 - imgwidth) + (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = true
            drawThis = false
            break
          case 'TC':
            top = ((0 - imgheight) + (offsetY * 2)) + 30
            left = 30
            doVertDrop = false
            doHorDrop = true
            drawThis = false
            break
          case 'TR':
            top = ((0 - imgheight) + (offsetY * 2)) + 30
            left = (imgwidth - (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = true
            drawThis = false
            break
          case 'ML':
            top = 30
            left = ((0 - imgwidth) + (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = false
            drawThis = false
            break
          case 'MC':
            top = 30
            left = 30
            doVertDrop = false
            doHorDrop = false
            drawThis = true
            break
          case 'MR':
            top = 30
            left = (imgwidth - (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = false
            drawThis = false
            break
          case 'BL':
            top = (imgheight - (offsetY * 2)) + 30
            left = (0 - imgwidth) + (offsetX * 2) + 30
            doVertDrop = true
            doHorDrop = true
            drawThis = true
            break
          case 'BC':
            top = (imgheight - (offsetY * 2)) + 30
            left = 30
            doVertDrop = false
            doHorDrop = true
            drawThis = true
            break
          case 'BR':
            top = (imgheight - (offsetY * 2)) + 30
            left = imgwidth - (offsetX * 2) + 30
            doVertDrop = true
            doHorDrop = true
            drawThis = true
            break
        }
        dropDirection === 'vertical' && doVertDrop ? top += imgheight / dropCount : top += imgheight / dropCount
        dropDirection === 'horizontal' && doHorDrop ? left += imgwidth / dropCount : left += imgwidth / dropCount
        context.drawImage(virtualCanvas, left, top, imgwidth, imgheight) // To whichever context drawToCanvas was called with, draw virtualCanvas data
      }
      if (typeof callback === 'function') {
        callback()
      }
    }
    /**
     * Overlays a mask when a background color is selected
     * @param {pixel} mask Color of selected pixel
     */
    var drawMaskCanvas = function (mask) {
      console.log('mask color:' + mask) // is this pixel the exact same as mask color?
      var transparentData = maskCtx.getImageData(0, 0, img_data.$selected.data('width'), img_data.$selected.data('height'))
      for (var i = 0 - transparentData.data.length; i < transparentData.data.length; i += 4) {
        if (transparentData.data[i] === parseInt(mask[0]) && transparentData.data[i - 1] === parseInt(mask[1]) && transparentData.data[i - 2] === parseInt(mask[2]) && transparentData.data[i - 3] === parseInt(mask[3])) {
          transparentData.data[i - 3] = 0
        }
        if (transparentData.data[i] === parseInt(mask[0]) && transparentData.data[i - 1] === parseInt(mask[1]) && transparentData.data[i + 2] === parseInt(mask[2]) && transparentData.data[i + 3] === parseInt(mask[3])) {
          transparentData.data[i + 3] = 0
        }
      }
      virtualCtx.putImageData(transparentData, 0, 0) // change transparency to 0
      drawToCanvas(ctx, canvasElement, true) // put the altered data back on the canvas  // master canvas is drawn
    }
    /**
     * Clears a canvas given its context and dimensions
     * @param {Canvas.context} context The canvas 2d rendering context
     * @param {Number} width Canvas width
     * @param {Number} height Canvas height
     */
    var clearCanvas = function (context, width, height) {
      context.clearRect(0, 0, width, height)
    }
    /**
     * Sets sliders to reasonable starting values
     */
    var reposition_sliders = function () {
      $horizontalSlider.parent().css('width', function () {
        return $(canvasElement).width() / 2
      })
      $horizontalSlider.parent().css('margin-left', function () {
        return -($(this).width() / 2)
      })
      $verticalSlider.parent().css('width', function () {
        return $(canvasElement).height() / 2
      })
      $verticalSlider.parent().css('left', function () {
        var left = (window.innerWidth / 2) - (img_data.scaled_width / 2) - ($(this).width() / 2) - 55
        return left
      })
    }
    /**
     * Creates a canvas with image repeat data
     * @returns {Canvas} Repeat canvas
     */
    var getRepeatCanvas = function () {
      var width = $horizontalSlider.val()
      var height = $verticalSlider.val()
      var offsetX = (img_data.width - $horizontalSlider.val()) / 2
      var offsetY = (img_data.height - $verticalSlider.val()) / 2
      var tempCanvas = document.createElement('canvas')
      var imageCanvas = document.createElement('canvas')
      tempCanvas.id = 'tempCanvas'
      imageCanvas.id = 'imageCanvas'
      var tempCtx = tempCanvas.getContext('2d')
      var imgCtx = imageCanvas.getContext('2d')
      var r = maskColor[0]
      var g = maskColor[1]
      var b = maskColor[2]
      var a = maskColor[3]
      var rgba = 'rgba(' + r + ', ' + g + ',' + b + ',' + a + ')'
      tempCanvas.width = width
      tempCanvas.height = height
      imageCanvas.width = img_data.width
      imageCanvas.height = img_data.height
      imgCtx.fillStyle = rgba
      imgCtx.fillRect(0, 0, img_data.width, img_data.height)
      drawToCanvas(imageCanvas.getContext('2d'), imageCanvas, false, false) // imageCanvas is drawn
      var imageData = imgCtx.getImageData(offsetX, offsetY, width, height)
      tempCtx.putImageData(imageData, 0, 0)
      return tempCanvas
    }
    /**
     * Uses tempCanvas to save repeated image to PNG format
     * @param {Anchor} anchor Blank window href to use for saving image
     */
    var saveToPNG = function (anchor) {
      var filename = img_data.activeImage.split('/').pop()
      var new_filename = 'REPEAT ' + filename
      var tempCanvas = getRepeatCanvas()
      anchor.href = tempCanvas.toDataURL('image/png')
      anchor.download = new_filename
    }
    /**
     * Uses tempCanvas to save image to server
     * Calls img-upload script
     */
    var saveToServer = function () {
      var tempCanvas = getRepeatCanvas()
      var dataURL = tempCanvas.toDataURL('image/png')
      var img_base64 = dataURL.split(',')
      var filename = img_data.activeImage.split('/').pop()
      var dateInMS = Date.now()
      var new_filename = 'REPEAT_' + dateInMS + '_' + filename
      var xhr = new XMLHttpRequest()
      var formData = new FormData()
      var siteurl = 'https://w2ftest.web2fabric.com/'
      w2f_settings.debug ? siteurl = w2f_settings.debug_url : siteurl = w2f_settings.debug_url
      !$('.img-upload').hasClass('in') ? $('#img-upload-wrap').collapse('toggle') : $('#img-upload-wrap').collapse('toggle')
      add_mock_image()
      formData.append('cid', w2f_settings.customer)
      formData.append('shop', document.location.host)
      formData.append('image', img_base64[img_base64.length - 1])
      formData.append('image_size', [img_data.width, img_data.height])
      formData.append('filename', new_filename)
      formData.append('dpi', img_data.$selected.data('dpi'))
      xhr.open('POST', siteurl + 'upload/save/', true)
      xhr.onreadystatechange = function (e) {
        if (this.readyState === 4) {
          if (this.status === 200) {
            var json = JSON.parse(this.response)
            var width = json.width
            var height = json.height
            var id = 'image' + json.id
            var buttonID = 'removeImg' + json.id
            var dpi = json.dpi
            var thumb = json.thumb
            var full_img = json.full_img
            $('.img-thumb.loading input').val(full_img).data('width', width).data('height', height)
            $('.img-thumb.loading input').data('thumb', thumb).data('dpi', dpi).attr('id', id)
            $('.img-thumb.loading label').attr('for', id)
            $('.img-thumb.loading button').attr('id', buttonID).attr('data-id', json.id).prop('disabled', false).on('click', function (e) {
              e.preventDefault()
              dj.removeUserImage(json.id)
            })
            $('.img-thumb.loading img').attr('src', thumb)
            $('.img-thumb.loading').removeClass('loading')
          } else {
            $('.img-thumb.loading').remove()
          }
        }
      }
      xhr.send(formData)
    }
    /**
     * Clones whichever image is selected and appends it to .uploaded-user-images
     * If .img-thumb.length === 5 disable #save-repeat
     */
    var add_mock_image = function () {
      var selected = $('.img-thumb input:checked').parents('.img-thumb')
      var clone = selected.clone()
      $(clone).addClass('loading').appendTo('.uploaded-user-images')
      $('.img-thumb').length === 5 ? $('#save-repeat').prop('disabled', true) : $('#save-repeat').prop('disabled', true)
    }
    /**
     * Sets nice round numbers for default slider values
     */
    var initiate_slider_values = function () {
      var minVert = Math.round(img_data.height / 2)
      var minHor = Math.round(img_data.width / 2)
      var maxVert = Math.round(img_data.height - 30)
      var maxHor = Math.round(img_data.width - 30)
      $horizontalSlider.attr('min', minHor).attr('max', maxHor).val(maxHor)
      $verticalSlider.attr('min', minVert).attr('max', maxVert).val(maxVert)
    }
    /**
     * Hides sliders
     */
    var hide_sliders = function () {
      $verticalSlider.parent().addClass('hidden')
      $horizontalSlider.parent().addClass('hidden')
    }
    /**
     * Removes style data from #mask-color and calls hide_sliders()
     */
    var resetMaskColor = function () {
      $('#mask-color').removeAttr('style')
      hide_sliders()
    }
    /**
     * Redraws/rescales master canvas and repositions sliders
     */
    var redraw_canvas = function () {
      resize_canvas()
      drawToCanvas(ctx, canvasElement, true)
      reposition_sliders()
    }
    return {
      init: init,
      set_image: setActiveImg,
      init_canvas: initiate_canvas
    }
  })(jQuery)
  return HFRepeatCreator
}
var HFRepeatCreator = HF_initiate_Repeat_Creator()
if (typeof $ === 'undefined') { // Only do anything if jQuery isn't defined
  var getScript = function (url, success) {
    var script = document.createElement('script')
    script.src = url
    var head = document.getElementsByTagName('head')[0]
    var done = false // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function () {
      if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
        done = true
        success() // callback function provided as param
        script.onload = script.onreadystatechange = null
        head.removeChild(script)
      }
    }
    head.appendChild(script)
  }
  getScript('//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', function () {
    HF_initiate_Repeat_Creator()
  })
} else {
  HFRepeatCreator.init()
}

function mm_userimages_callback (count) {
  $('.user-image input:checked').length === 0 ? $('.img-thumb:first input').prop('checked', true) : $('.img-thumb:first input').prop('checked', true)
  HFRepeatCreator.set_image($('.user-image input:checked'))
  HFRepeatCreator.init_canvas(false)
  count === 5 ? $('#save-repeat').prop('disabled', true) : $('#save-repeat').prop('disabled', false)
}

function mm_removesuccess_callback () {
  $('#save-repeat').prop('disabled', false)
}
