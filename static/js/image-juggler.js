/* eslint-disable camelcase,no-unused-vars */
/* globals $,jQuery,Image,FileReader,FormData,XMLHttpRequest,dj,w2f_settings,alert */
/* image-juggler 1.0.0|author: Drew Petersen <kirbysayshi@gmail.com>|license: MIT
repo: https://github.com/kirbysayshi/image-juggler */
/**
 * Draws an image to a given canvas
 *
 * @param {Image} image an image file
 * @param {Canvas} optCvs specify canvas
 * @param {Function} cb callback
 */
function imageToCanvas (image, optCvs, cb) {
  if (!cb) {
    cb = optCvs
    optCvs = null
  }
  var cvs = optCvs || document.createElement('canvas')
  var ctx = cvs.getContext('2d')
  cvs.width = image.width
  cvs.height = image.height
  ctx.drawImage(image, 0, 0)
  cb(null, cvs)
}

/**
 * Loads image to an array buffer
 * @param {Image} image an image file
 * @param {Function} cb callback
 */
function imageToArrayBuffer (image, cb) {
  var reader = new FileReader()
  reader.onload = function () {
    cb(reader.error, reader.result)
  }
  imageToCanvas(image, function (err, cvs) {
    if (!err) {
      cvs.toBlob(reader.readAsArrayBuffer.bind(reader))
    } else {
      console.log(err)
    }
  })
}

/**
 * Loads a generic file to an array buffer
 * @param {String} file a file path name
 * @param {Function} cb callback
 */
function fileToArrayBuffer (file, cb) {
  var reader = new FileReader()
  reader.onload = function () {
    cb(reader.error, reader.result)
  }
  reader.readAsArrayBuffer(file)
}

/**
 * Writes image data to an image object
 * @param {ImageData} imageData processed image data
 * @param {Image} optImage Image to fill with data
 * @param {Function} cb callback
 */
function imageDataToImage (imageData, optImage, cb) {
  if (!cb) {
    cb = optImage
    optImage = null
  }
  var img = optImage || document.createElement('img')
  var cvs = document.createElement('canvas')
  var ctx = cvs.getContext('2d')
  cvs.width = imageData.width
  cvs.height = imageData.height
  ctx.putImageData(imageData, 0, 0)
  canvasToImage(cvs, img, cb)
}

/**
 * Loads an image file to an image object
 * @param {FileReader*} file image file to load
 * @param {Image} optImage image object
 * @param {Function} cb callback
 */
function fileToImage (file, optImage, cb) {
  if (!cb) {
    cb = optImage
    optImage = null
  }
  var img = optImage || document.createElement('img')
  var url = URL.createObjectURL(file)
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  img.src = url
}

/**
 * Takes a snapshot of the canvas and writes it to an image
 * @param {Canvas*} canvas the canvas to read
 * @param {Image} optImage image to write canvas data to
 * @param {Function} cb callback
 */
function canvasToImage (canvas, optImage, cb) {
  if (!cb) {
    cb = optImage
    optImage = null
  }
  var url
  var img = optImage || document.createElement('img')
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  canvas.toBlob(function (blob) {
    url = URL.createObjectURL(blob)
    img.src = url
  })
}

/**
 *
 * @param {Blob} blob binary blob data
 * @param {Image} optImage image object to fill with data
 * @param {Function} cb callback
 */
function blobToImage (blob, optImage, cb) {
  if (!cb) {
    cb = optImage
    optImage = null
  }
  var img = optImage || document.createElement('img')
  var url = URL.createObjectURL(blob)
  img.onload = function () {
    URL.revokeObjectURL(url)
    cb(null, img)
  }
  img.src = url
}

/**
 * Paints a pixel array to a canvas
 * @param {Array} pixels
 * @param {Canvas} optCanvas
 * @param {Function} cb
 */
function pixelsToCanvas (pixels, optCanvas, cb) {
  if (!cb) {
    cb = optCanvas
    optCanvas = null
  }
  var cvs = optCanvas || document.createElement('canvas')
  var width = pixels.width || cvs.width
  var height = pixels.height || cvs.height
  cvs.width = width
  cvs.height = height
  var ctx = cvs.getContext('2d')
  var imgdata = ctx.createImageData(width, height)
  for (var i = 0; i < pixels.length; i++) {
    imgdata.data[i] = pixels[i]
  }
  ctx.putImageData(imgdata, 0, 0)
  cb(null, cvs)
}

// exports.imageToCanvas = imageToCanvas
// exports.imageToArrayBuffer = imageToArrayBuffer
// exports.imageDataToImage = imageDataToImage
// exports.fileToArrayBuffer = fileToArrayBuffer
// exports.fileToImage = fileToImage
// exports.canvasToImage = canvasToImage
// exports.blobToImage = blobToImage
// exports.pixelsToCanvas = pixelsToCanvas
