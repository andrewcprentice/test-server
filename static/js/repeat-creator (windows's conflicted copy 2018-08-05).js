// Repeat creator function that handles a number of things:
// - Load the image onto the HTML5 canvas
// - Create a new repeat by adjusting horizontal and vertical positioning of 8 quadrants also containing the design
// - Allow the result to be saved to the user library
// - Allow the result to be downloaded
function HF_initiate_Repeat_Creator () {
  var HFRepeatCreator = (function ($) { // initiate variables, cache DOM elements
    var canvasElement = document.createElement('canvas')
    canvasElement.id = 'repeat-creator-canvas'
    var ctx = canvasElement.getContext('2d')
    var repeatCreator = $('.repeat-creator-container')
    repeatCreator[0].appendChild(canvasElement)
    var $horizontalSlider = $('#horizontal-adjust')
    var $verticalSlider = $('#vertical-adjust')
    var selectColor = false // State variables
    var resizeTimer = 0
    var maskColor = []
    var img_data = {}
    var dropDirection = 'None'
    var dropCount = 0
    var sections = ['TL', 'TC', 'TR', 'ML', 'MR', 'BL', 'BC', 'BR']
    var TCInc = $('#TCInc')
    var TLInc = $('#TLInc')
    var TLBO = $('#TLBO')
    var TCBO = $('#TCBO')
    var TRBO = $('#TRBO')
    var TRInc = $('#TRInc')
    var MLInc = $('#MLInc')
    var MLBO = $('#MLBO')
    var MRInc = $('#MRInc')
    var MRBO = $('#MRBO')
    var BLInc = $('#BLInc')
    var BLBO = $('#BLBO')
    var BCInc = $('#BCInc')
    var BCBO = $('#BCBO')
    var BRInc = $('#BRInc')
    var BRBO = $('#BRBO')
    var virtualCanvas = document.createElement('canvas') // Virtual Canvases
    var maskCanvas = document.createElement('canvas')
    virtualCanvas.id = 'virtualCanvas'
    maskCanvas.id = 'maskCanvas'
    var virtualCtx = virtualCanvas.getContext('2d')
    var maskCtx = maskCanvas.getContext('2d')
    var init = function () {
      $(document).on('ready', function (e) { /** *** Set click events */
        $('#mask-color').on('click', function (e) { // Toggle Mask Color selection on
          e.preventDefault()
          selectColor = true
        })
        // Set Mask Color, unhide sliders, toggle Mask Color selection off
        $('body').on('click', function (e) {
          if (e.target.id === 'repeat-creator-canvas' && selectColor) {
            maskColor = e.target.getContext('2d').getImageData(e.offsetX, e.offsetY, 1, 1).data
            var rgba = 'rgba(' + maskColor.join(',') + ')'
            if (rgba === 'rgba(0,0,0,0)') {
              rgba = $('#repeat-creator-canvas').css('background-color').replace('rgb(', 'rgba(').replace(')', ', 255)')
              maskColor = rgba.replace('rgba(', '').replace(')', '').split(', ')
            }
            $('#mask-color').css('background-color', rgba)
            $('#repeat-creator canvas').css('background-color', rgba)
            show_sliders()
            selectColor = false // Redraw canvas (mask)
            drawMaskCanvas(maskColor) // Calling drawMaskCanvas with an Array
          }
        })
        $('#TLBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#TLInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#TCBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#TRBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#TRInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#MLBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#MLInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#MCBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#MCInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#MRBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#MRInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#BLBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#BLInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#BCBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#BCInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) })
        $('#BRBO').change(function () { drawToCanvas(ctx, canvasElement, false, true) }) // bottom right include
        $('#BRInc').change(function () { drawToCanvas(ctx, canvasElement, false, true) }) // User image selector toggle
        $('input[type="range"]').rangeslider({
          polyfill: true,
          onInit: function () { drawToCanvas(ctx, canvasElement, false, true) },
          onSlide: function () { drawToCanvas(ctx, canvasElement, false, true) },
          onSlideEnd: function () { drawToCanvas(ctx, canvasElement, false, true) }
        })
        $('#select-image').click(function (e) { $('#img-upload-wrap').collapse('toggle') })
        $('#img-upload-wrap').on('show.bs.collapse', function (e) { $('#select-image small').text('Hide My Designs') })
        $('#img-upload-wrap').on('hide.bs.collapse', function (e) { $('#select-image small').text('Show My Designs') }) // User image click
        $('.user-images').click(function (e) {
          var target = $(e.target)
          if (target.is('input[type="radio"]')) { // Load image to canvas
            img_data.activeImage = $(target).data('thumb')
            img_data.$selected = $(target)
            loadImage()
          }
        })
        $('.repeat-adjust').click(function (e) { // Slider button click
          var target = $(e.target)
          if (target.is('button')) {
            e.preventDefault()
            if (target.hasClass('increment')) {
              target.siblings('input').val(function (index, value) {
                return parseInt(value) + parseInt($(this).attr('step'))
              })
              $horizontalSlider.trigger('change')
            } else if (target.hasClass('decrement')) {
              target.siblings('input').val(function (index, value) {
                return parseInt(value) - parseInt($(this).attr('step'))
              })
              $horizontalSlider.trigger('change')
            }
          }
        })
        $('#download-repeat').click(function (e) {
          saveToPNG(this)
        })
        $('#save-repeat').click(function (e) {
          e.preventDefault()
          saveToServer()
        })
        $('#repeat-drop').on('change', function (e) { // Change drop value
          var $selectedOption = $(this).find('option:selected')
          dropDirection = $selectedOption.data('offset')
          dropCount = $selectedOption.data('value') // Redraw canvas (drop change)
          drawToCanvas(ctx, canvasElement, true)
        }) // Slider changes
        $horizontalSlider.on('change input', function (e) {
          drawToCanvas(ctx, canvasElement, true)
        })
        $verticalSlider.on('change input', function (e) {
          drawToCanvas(ctx, canvasElement, true)
        })
        $(window).on('resize', function (e) {
          clearTimeout(resizeTimer)
          resizeTimer = setTimeout(redraw_canvas, 100)
        })
      })
    }
    var setActiveImg = function (image) {
      img_data.activeImage = image.data('thumb')
      img_data.$selected = image
    }
    var resize_canvas = function () {
      img_data.width = img_data.$selected.data('width')
      img_data.height = img_data.$selected.data('height')
      if (img_data.width > img_data.height) { // Resize canvas based on window / image size
        img_data.scaled_width = (window.innerWidth * 0.75)
        img_data.scale = img_data.scaled_width / img_data.width
        img_data.scaled_height = img_data.height * img_data.scale
        if (img_data.scaled_height > window.innerHeight) { // Re-adjust in case the height becomes more than the window's height
          img_data.scaled_height = window.innerHeight * 0.8 - $('#repeat-creator-toolbar').innerHeight()
          img_data.scale = img_data.scaled_height / img_data.height
          img_data.scaled_width = img_data.width * img_data.scale
        }
      } else {
        if (window.innerWidth > window.innerHeight) {
          img_data.scaled_height = (window.innerHeight * 0.85) - $('#repeat-creator-toolbar').innerHeight()
          img_data.scale = img_data.scaled_height / img_data.height
          img_data.scaled_width = img_data.width * img_data.scale
        } else {
          img_data.scaled_width = (window.innerWidth * 0.75)
          img_data.scale = img_data.scaled_width / img_data.width
          img_data.scaled_height = img_data.height * img_data.scale
        }
      }
      $(canvasElement).width(img_data.scaled_width + 60)
      $(canvasElement).height(img_data.scaled_height + 60)
      initiate_slider_values()
      canvasElement.width = img_data.scaled_width + 60
      canvasElement.height = img_data.scaled_height + 60
    }
    var loadImage = function () {
      var img = new Image()
      img.crossOrigin = 'Anonymous'
      resetMaskColor()
      img.onload = function () { // Store image data
        resize_canvas()
        virtualCanvas.width = img_data.$selected.data('width')
        virtualCanvas.height = img_data.$selected.data('height')
        maskCanvas.width = img_data.$selected.data('width')
        maskCanvas.height = img_data.$selected.data('height')
        virtualCtx.drawImage(this, 0, 0, img_data.$selected.data('width'), img_data.$selected.data('height'))
        maskCtx.drawImage(this, 0, 0, img_data.$selected.data('width'), img_data.$selected.data('height'))
        drawToCanvas(ctx, canvasElement, true)
        reposition_sliders()
      }
      img.src = img_data.activeImage
    }
    var drawToCanvas = function (context, canvas, clear, scaled, callback) {
      var top = void 0
      var left = void 0
      var imgheight = img_data.scaled_height
      var imgwidth = img_data.scaled_width
      var offsetX = (img_data.scaled_width - ($horizontalSlider.val()) * img_data.scale) / 2
      var offsetY = (img_data.scaled_height - ($verticalSlider.val()) * img_data.scale) / 2
      if (!scaled && scaled !== undefined) {
        imgheight = img_data.height
        imgwidth = img_data.width
        offsetX = (imgwidth - $horizontalSlider.val()) / 2
        offsetY = (imgheight - $verticalSlider.val()) / 2
      }
      if (clear) {
        clearCanvas(context, canvas.width, canvas.height)
      }
      var doVertDrop = false
      var doHorDrop = false
      var drawThis = false
      var maskOnly = false
      for (var section in sections) {
        switch (sections[section]) {
          case 'TL':
            top = ((0 - imgheight) + (offsetY * 2)) + 30
            left = ((0 - imgwidth) + (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = true
            if (TLInc[0].checked) {
              drawThis = true
            }
            if (TLBO[0].checked) {
              maskOnly = true
            }
            break
          case 'TC':
            top = ((0 - imgheight) + (offsetY * 2)) + 30
            left = 30
            doVertDrop = false
            doHorDrop = true
            if (TCInc[0].checked) {
              drawThis = true
            }
            if (TCBO[0].checked) {
              maskOnly = true
            }
            break
          case 'TR':
            top = ((0 - imgheight) + (offsetY * 2)) + 30
            left = (imgwidth - (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = true
            if (TRInc[0].checked) {
              drawThis = true
            }
            if (TRBO[0].checked) {
              maskOnly = true
            }
            break
          case 'ML':
            top = 30
            left = ((0 - imgwidth) + (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = false
            if (MLInc[0].checked) {
              drawThis = true
            }
            if (MLBO[0].checked) {
              maskOnly = true
            }
            break
          case 'MR':
            top = 30
            left = (imgwidth - (offsetX * 2)) + 30
            doVertDrop = true
            doHorDrop = false
            if (MRInc[0].checked) {
              drawThis = true
            }
            if (MRBO[0].checked) {
              maskOnly = true
            }
            break
          case 'BL':
            top = (imgheight - (offsetY * 2)) + 30
            left = (0 - imgwidth) + (offsetX * 2) + 30
            doVertDrop = true
            doHorDrop = true
            if (BLInc[0].checked) {
              drawThis = true
            }
            if (BLBO[0].checked) {
              maskOnly = true
            }
            break
          case 'BC':
            top = (imgheight - (offsetY * 2)) + 30
            left = 30
            doVertDrop = false
            doHorDrop = true
            if (BCInc[0].checked) {
              drawThis = true
            }
            if (BCBO[0].checked) {
              maskOnly = true
            }
            break
          case 'BR':
            top = (imgheight - (offsetY * 2)) + 30
            left = imgwidth - (offsetX * 2) + 30
            doVertDrop = true
            doHorDrop = true
            if (BRInc[0].checked) {
              drawThis = true
            }
            if (BRBO[0].checked) {
              maskOnly = true
            }
            break
        }
        if (dropDirection === 'vertical' && doVertDrop) {
          top += imgheight / dropCount
        }
        if (dropDirection === 'horizontal' && doHorDrop) {
          left += imgwidth / dropCount
        }
        if (drawThis) {
          context.drawImage(virtualCanvas, left, top, imgwidth, imgheight)
        }
        if (maskOnly) {
          context.drawImage(maskCanvas, left, top, imgwidth, imgheight)
        }
      }
      if (typeof (callback) === 'function') {
        callback()
      }
    }
    /**
     * Gets an ImageData object from maskCtx and
     * checks each pixel to see if it matches mask pixel
     * calls maskCtx.getImageData(),virtualCtx.putImageData()
     * calls drawToCanvas(ctx, canvasElement, true)
     * @param {Array} mask An rgba array of 0-255 range
     */
    var drawMaskCanvas = function (mask) { // value of argument usually rgba(255,255,255,255)
      var transparentData = maskCtx.getImageData(0, 0, img_data.$selected.data('width'), img_data.$selected.data('height'))
      for (var i = 0; i < transparentData.data.length; i += 4) { // is this pixel the exact same as mask color?
        if (transparentData.data[i] === mask[0] &&
          transparentData.data[i + 1] === mask[1] &&
          transparentData.data[i + 2] === mask[2] &&
          transparentData.data[i + 3] === mask[3]) {
          transparentData.data[i + 3] = 0 // set alpha channel to 0 for transparent
        }
      }
      virtualCtx.putImageData(transparentData, 0, 0) // put the altered data back on the canvas
      drawToCanvas(ctx, canvasElement, true) // clears canvas
    }
    var clearCanvas = function (context, width, height) {
      context.clearRect(0, 0, width, height)
    }
    var reposition_sliders = function () {
      $horizontalSlider.parent().css('width', function () {
        return $(canvasElement).width() / 2
      })
      $horizontalSlider.parent().css('margin-left', function () {
        return -($(this).width() / 2)
      })
      $verticalSlider.parent().css('width', function () {
        return $(canvasElement).height() / 2
      })
      $verticalSlider.parent().css('left', function () {
        var left = (window.innerWidth / 2) - (img_data.scaled_width / 2) - ($(this).width() / 2) - 55
        return left
      })
    }
    var getRepeatCanvas = function () {
      var width = $horizontalSlider.val()
      var height = $verticalSlider.val()
      var offsetX = (img_data.width - $horizontalSlider.val()) / 2
      var offsetY = (img_data.height - $verticalSlider.val()) / 2
      var tempCanvas = document.createElement('canvas')
      var imageCanvas = document.createElement('canvas')
      tempCanvas.id = 'tempCanvas'
      imageCanvas.id = 'imageCanvas'
      var tempCtx = tempCanvas.getContext('2d')
      var imgCtx = imageCanvas.getContext('2d')
      var r = maskColor[0]
      var g = maskColor[1]
      var b = maskColor[2]
      var a = maskColor[3]
      var rgba = 'rgba(' + r + ', ' + g + ',' + b + ',' + a + ')'
      tempCanvas.width = width
      tempCanvas.height = height
      imageCanvas.width = img_data.width
      imageCanvas.height = img_data.height
      imgCtx.fillStyle = rgba
      imgCtx.fillRect(0, 0, img_data.width, img_data.height)
      drawToCanvas(imgCtx, imageCanvas, false, false)
      var imageData = imgCtx.getImageData(offsetX, offsetY, width, height)
      tempCtx.putImageData(imageData, 0, 0)
      return tempCanvas
    }
    var saveToPNG = function (anchor) {
      var filename = img_data.activeImage.split('/').pop()
      var new_filename = 'REPEAT ' + filename
      var tempCanvas = getRepeatCanvas() // returns tempCanvas
      anchor.href = tempCanvas.toDataURL('image/png')
      anchor.download = new_filename
    }
    var saveToServer = function () {
      var tempCanvas = getRepeatCanvas() // returns tempCanvas
      var dataURL = tempCanvas.toDataURL('image/png')
      var img_base64 = dataURL.split(',')
      var filename = img_data.activeImage.split('/').pop()
      var dateInMS = Date.now()
      var new_filename = 'REPEAT_' + dateInMS + '_' + filename
      var xhr = new XMLHttpRequest()
      var formData = new FormData()
      var siteurl = 'https://w2ftest.web2fabric.com/'
      if (w2f_settings.debug) {
        siteurl = w2f_settings.debug_url
      }
      if (!$('.img-upload').hasClass('in')) {
        $('#img-upload-wrap').collapse('toggle')
      }
      add_mock_image()
      formData.append('cid', w2f_settings.customer)
      formData.append('shop', document.location.host)
      formData.append('image', img_base64[img_base64.length - 1])
      formData.append('image_size', [img_data.width, img_data.height])
      formData.append('filename', new_filename)
      formData.append('dpi', img_data.$selected.data('dpi'))
      xhr.open('POST', siteurl + 'upload/save/', true)
      xhr.onreadystatechange = function (e) {
        if (this.readyState === 4) {
          if (this.status === 200) {
            var json = JSON.parse(this.response)
            var width = json.width
            var height = json.height
            var id = 'image' + json.id
            var buttonID = 'removeImg' + json.id
            var dpi = json.dpi
            var thumb = json.thumb
            var full_img = json.full_img
            $('.img-thumb.loading input').val(full_img).data('width', width).data('height', height)
            $('.img-thumb.loading input').data('thumb', thumb).data('dpi', dpi).attr('id', id)
            $('.img-thumb.loading label').attr('for', id)
            $('.img-thumb.loading button').attr('id', buttonID).attr('data-id', json.id).prop('disabled', false).on('click', function (e) {
              e.preventDefault()
              dj.removeUserImage(json.id)
            })
            $('.img-thumb.loading img').attr('src', thumb)
            $('.img-thumb.loading').removeClass('loading')
          } else {
            $('.img-thumb.loading').remove()
          }
        }
      }
      xhr.send(formData)
    }
    var add_mock_image = function () {
      var selected = $('.img-thumb input:checked').parents('.img-thumb')
      var clone = selected.clone()
      $(clone).addClass('loading').appendTo('.uploaded-user-images')
      if ($('.img-thumb').length === 5) {
        $('#save-repeat').prop('disabled', true)
      }
    }
    var initiate_slider_values = function () {
      var minVert = Math.round(img_data.height / 2)
      var minHor = Math.round(img_data.width / 2)
      var maxVert = Math.round(img_data.height - 30)
      var maxHor = Math.round(img_data.width - 30)
      $horizontalSlider.attr('min', minHor).attr('max', maxHor).val(maxHor)
      $verticalSlider.attr('min', minVert).attr('max', maxVert).val(maxVert)
    }
    var hide_sliders = function () {
      $verticalSlider.parent().addClass('hidden')
      $horizontalSlider.parent().addClass('hidden')
    }
    var show_sliders = function () {
      $verticalSlider.parent().removeClass('hidden')
      $horizontalSlider.parent().removeClass('hidden')
    }
    var resetMaskColor = function () {
      $('#mask-color').removeAttr('style')
      hide_sliders()
    }
    var redraw_canvas = function () {
      resize_canvas()
      drawToCanvas(ctx, canvasElement, true)
      reposition_sliders()
    }
    return {
      init: init,
      set_image: setActiveImg,
      init_canvas: loadImage
    }
  })(jQuery)
  return HFRepeatCreator
}
if (typeof jQuery === 'undefined') { // Only do anything if jQuery isn't defined
  var getScript = function (url, success) {
    var script = document.createElement('script')
    script.src = url
    var head = document.getElementsByTagName('head')[0]
    var done = false // Attach handlers for all browsers
    script.onload = script.onreadystatechange = function () {
      if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
        done = true
        success() // callback function provided as param
        script.onload = script.onreadystatechange = null
        head.removeChild(script)
      }
    }
    head.appendChild(script)
  }
  getScript('//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js', function () {
    HF_initiate_Repeat_Creator()
  })
} else { // jQuery was already loaded Run your jQuery Code
  var HFRepeatCreator = HF_initiate_Repeat_Creator()
  HFRepeatCreator.init()
}
function mm_userimages_callback (count) {
  if (jQuery('.user-image input:checked').length === 0) {
    jQuery('.img-thumb:first input').prop('checked', true)
  }
  HFRepeatCreator.set_image(jQuery('.user-image input:checked'))
  HFRepeatCreator.init_canvas(false)
  if (count === 5) {
    $('#save-repeat').prop('disabled', true)
  } else {
    $('#save-repeat').prop('disabled', false)
  }
}
function mm_removesuccess_callback () {
  $('#save-repeat').prop('disabled', false)
}
